import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};
		int counter = 0;         // to check draw situation
		printBoard(board);

		//Player 1's turn
		while (true){
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();
			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

			while ( (row < 1 || row > 3) || (col < 1 || col > 3 ) || (board[row-1][col-1] != ' ')) {//wrong coordinates
				System.out.println("Your coordinates are not reachable!!Try new ones!!");
				System.out.print("Player 1 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 1 enter column number:");
				col = reader.nextInt();
			}
			board[row - 1][col - 1] = 'X';
			printBoard(board);
			counter++;
			if (checkBoard(board)) {
				System.out.println("Player 1 wins!!");
				break;
			}
			if (counter >= 9) { // There are at most 9 moves.
				System.out.println("Draw!!");
				break;
			}


			//Player 2's turn
			System.out.print("Player 2 enter row number:");
			row = reader.nextInt();
			System.out.print("Player 2 enter column number:");
			col = reader.nextInt();
			while ( (row < 1 || row > 3) || (col < 1 || col > 3 ) || (board[row-1][col-1] != ' ')) {
				System.out.println("Your coordinates are not reachable!!Try new ones!!");
				System.out.print("Player 2 enter row number:");
				row = reader.nextInt();
				System.out.print("Player 2 enter column number:");
				col = reader.nextInt();
			}
			board[row - 1][col - 1] = 'O';
			printBoard(board);
			counter++;
			if (checkBoard(board)) {
				System.out.println("Player 2 wins!!");
				break;
			}
		}
		reader.close();
	}


	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			}
			System.out.println();
			System.out.println("   -----------");
		}
	}


	public static boolean checkBoard(char[][] board) {
		for (int i=0; i<3; i++) { // if for vertical checking and else if for horizontal checking
			if ((board[0][i] != ' ' && board[1][i] == board[0][i]) && board[2][i] == board[0][i])
				return true;
			else if (board[i][0] != ' ' && board[i][0] == board[i][2] && board[i][0] == board[i][1])
				return true;

		// that if and else for cross checking
		}if ( board[0][2] != ' ' && board[0][2] == board[2][0] && board[0][2] == board[1][1]) {
			return true;
		}else return board[0][0] != ' ' && board[0][0] == board[1][1] && board[0][0] == board[2][2];
	}
}
